import 'package:covid_risk_calculator/data/database/database.dart';
import 'package:covid_risk_calculator/ui/screen/about-scree.dart';
import 'package:covid_risk_calculator/ui/screen/choices/result-page.dart';
import 'package:covid_risk_calculator/ui/screen/test-screen.dart';
import 'package:covid_risk_calculator/ui/widgets/le-button.dart';
import 'package:covid_risk_calculator/utils/date/date_helpers.dart';
import 'package:covid_risk_calculator/utils/getIt.dart';
import 'package:covid_risk_calculator/utils/ui/common/ui.dart';
import 'package:covid_risk_calculator/utils/ui/common/widgets.dart';
import 'package:covid_risk_calculator/utils/ui/nav.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Widget> _resolver(BuildContext context, List<Result> results) {
      if (results.isEmpty) {
        return [
          SliverFillRemaining(
            child: Center(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  CommonWidgets.masker(Icon(
                    MdiIcons.listStatus,
                    size: 100,
                  )),
                  SizedBox(
                    height: 15,
                  ),
                  Text(
                    "You haven’t completed any\n risk calculations yet!",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  LeButton(
                    onTap: () {
                      navigateToScreen(context, screen: TestScreen());
                    },
                    label: "Calculate",
                  ),
                ],
              ),
            ),
          )
        ];
      } else {
        return [
          SliverToBoxAdapter(
              child: Column(
            children: [
              SizedBox(
                height: 20,
              ),
              Text(
                "Previous Results",
                style: TextStyle(fontSize: 20),
              ),
              // SizedBox(
              //   height: 10,
              // ),
              // Center(
              //   child: Text(
              //     "Calculations based on COVID-19 statistics from",
              //     textAlign: TextAlign.center,
              //     style: TextStyle(fontSize: 18),
              //   ),
              // ),
              // Divider(),
            ],
          )),
          SliverList(
              delegate: SliverChildBuilderDelegate((context, index) {
            var _item = results[index];
            return Container(
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                    border: Border.all(color: UiCommons.primaryColor),
                    borderRadius: BorderRadius.circular(10)),
                margin: EdgeInsets.all(10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Calculated on " + beautifiedDate(_item.date),
                      style: TextStyle(
                          // color: UiCommons.primaryColor,

                          fontSize: 15),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Wrap(
                      spacing: 10,
                      children: [
                        Text(
                          ((_item.result).roundToPrecision(2)).toString() + "%",
                          style: TextStyle(
                              color: UiCommons.primaryColor, fontSize: 20),
                        ),
                        Text(
                          riskToWarning(_item.result),
                          style: TextStyle(
                              color: UiCommons.accentColor, fontSize: 16),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 5,
                    ),
                  ],
                ));
          }, childCount: results.length))
        ];
      }
    }

    return StreamBuilder<List<Result>>(
        initialData: [],
        stream: getIt.get<CovidDatabase>().resultsDao.all(),
        builder: (context, snapshot) {
          var _result = snapshot.data!;
          return Scaffold(
              floatingActionButtonLocation:
                  FloatingActionButtonLocation.centerFloat,
              floatingActionButton:
                  Wrap(alignment: WrapAlignment.center, children: [
                LeButton(
                    color: UiCommons.primaryColor,
                    width: 150,
                    onTap: () {
                      navigateToScreen(context, screen: AboutScreen());
                    },
                    label: "About"),
                _result.isEmpty
                    ? Container()
                    : LeButton(
                        width: 150,
                        onTap: () {
                          navigateToScreen(context, screen: TestScreen());
                        },
                        label: "Calculate",
                      )
              ]),

              // appBar: AppBar(
              //     actions: [
              //       IconButton(
              //         onPressed: () {},
              //         icon: Icon(Icons.info),
              //         iconSize: 30,
              //         color: UiCommons.primaryColor,
              //       )
              //     ],
              //     centerTitle: true,
              //     elevation: 0,
              //     backgroundColor: Colors.white,
              //     title: CommonWidgets.masker(Text(
              //       "Home",
              //       style: TextStyle(
              //           fontSize: 25,
              //           fontWeight: FontWeight.w900,
              //           color: UiCommons.primaryColor),
              //     )),
              //     automaticallyImplyLeading: false),
              body: CustomScrollView(
                slivers: [
                  SliverToBoxAdapter(
                      child: SafeArea(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        SizedBox(
                          height: 30,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              padding: EdgeInsets.all(20),
                              decoration: BoxDecoration(
                                gradient: UiCommons.leftToRight(),
                                borderRadius: BorderRadius.circular(20),
                              ),
                              child: Icon(
                                MdiIcons.virus,
                                size: 70,
                                color: Colors.white,
                              ),
                            ),
                            SizedBox(
                              width: 20,
                            ),
                            Column(
                              children: [
                                Text(
                                  "COVID-19",
                                  style: TextStyle(
                                      fontSize: 30,
                                      fontWeight: FontWeight.w900,
                                      color: UiCommons.primaryColor),
                                ),
                                SizedBox(
                                  height: 2,
                                ),
                                Text(
                                  "Risk Assessment",
                                  style: TextStyle(
                                      fontSize: 25,
                                      fontWeight: FontWeight.w900,
                                      color: UiCommons.primaryColor),
                                ),
                              ],
                            )
                          ],
                        )
                      ],
                    ),
                  )),
                  ..._resolver(context, _result)
                ],
              ));
        });
  }
}
