import 'package:covid_risk_calculator/ui/screen/home-screen.dart';
import 'package:covid_risk_calculator/utils/ui/common/ui.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(seconds: 3), () {
      Navigator.of(context)
          .pushReplacement(MaterialPageRoute(builder: (ctx) => HomeScreen()));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: _Splash());
  }
}

class _Splash extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      extendBody: true,
      body: Container(
          decoration: BoxDecoration(gradient: UiCommons.topToBottom()),
          child: Center(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Spacer(),
              Icon(
                MdiIcons.virus,
                color: Colors.white,
                size: 150,
              ),
              Icon(
                MdiIcons.pulse,
                color: Colors.white,
                size: 40,
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                "COVID-19 Risk\nCalculator",
                textAlign: TextAlign.center,
                style: TextStyle(
                    letterSpacing: 1,
                    fontSize: 30,
                    color: Colors.white,
                    fontWeight: FontWeight.bold),
              ),
              Spacer(),
              CircularProgressIndicator(
                color: Colors.white,
                strokeWidth: 1.5,
              ),
              SizedBox(
                height: 50,
              ),
            ],
          ))),
    );
  }
}
