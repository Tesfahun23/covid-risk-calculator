import 'package:covid_risk_calculator/data/database/database.dart';
import 'package:covid_risk_calculator/ui/screen/choices/choice-five.dart';
import 'package:covid_risk_calculator/ui/screen/choices/choice-four.dart';
import 'package:covid_risk_calculator/ui/screen/choices/choice-one.dart';
import 'package:covid_risk_calculator/ui/screen/choices/choice-seven.dart';
import 'package:covid_risk_calculator/ui/screen/choices/choice-six.dart';
import 'package:covid_risk_calculator/ui/screen/choices/choice-three.dart';
import 'package:covid_risk_calculator/ui/screen/choices/choice-two.dart';
import 'package:covid_risk_calculator/ui/screen/choices/result-page.dart';
import 'package:covid_risk_calculator/utils/getIt.dart';
import 'package:covid_risk_calculator/utils/gsheet.dart';
import 'package:covid_risk_calculator/utils/ui/common/widgets.dart';
import 'package:covid_risk_calculator/utils/ui/nav.dart';
import 'package:drift/drift.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:logger/logger.dart';

final _db = getIt.get<CovidDatabase>();

class TestScreen extends StatefulWidget {
  TestScreen({Key? key}) : super(key: key);

  @override
  _TestScreenState createState() => _TestScreenState();
}

class _TestScreenState extends State<TestScreen> {
  late PageController _pageController;

  List<Widget> _pages = [];
  int _current = 0;

  double _a = 0;
  double _b = 0;
  double _c = 0;
  double _d = 0;

  double _result = 0;

  List<double> _five = [];
  List<double> _six = [];
  List<double> _seven = [];

  double _calculateResult() {
    var _data = _a +
        _b +
        _c +
        _d -
        (_a * _b) -
        (_a * _c) -
        (_a * _d) -
        (_b * _c) -
        (_d * _b) -
        (_d * _c) +
        (_a * _b * _c * _d);

    return _data;
  }

  void _finalize() {
    var _res = _calculateResult();

    for (var i = 0; i < _five.length; i++) {
      _res *= _five[i];
    }

    Logger().w(_res);

    if (_res > 1) {
      _res = 0.99;
    }

    for (var i = 0; i < _six.length; i++) {
      if (_six[i] == 1 / 10000) {
        _res += _six[i];
      } else {
        _res *= _six[i];
      }
    }
    Logger().w(_res);
    if (_res > 1) {
      _res = 0.99;
    }

    for (var i = 0; i < _seven.length; i++) {
      _res *= _seven[i];
    }

    Logger().w(_res);

    if (_res > 1) {
      setState(() {
        _result = 0.99 * 100;
      });
    } else {
      setState(() {
        _result = _res * 100;
      });
    }

    _addResult();
  }

  Future _addResult() async {
    await _db.resultsDao.addResult(
        ResultsCompanion(result: Value(_result), date: Value(DateTime.now())));
    await addResultToSheet(_result);
  }

  @override
  void initState() {
    _pageController = new PageController(initialPage: 0);
    _initPages();
    super.initState();
  }

  void reset() {
    setState(() {
      _a = 0;
      _b = 0;
      _c = 0;
      _d = 0;
      _result = 0;
      _five = [];
      _six = [];
      _seven = [];
    });
    _pageController.jumpToPage(0);

    _initPages();
  }

  void _initPages() {
    setState(() {
      _pages = [
        ChoiceOne(
          onTap: (x) {
            setState(() {
              _a = x;
            });
            _pageController.nextPage(
                duration: Duration(
                  milliseconds: 100,
                ),
                curve: Curves.easeInCirc);
          },
        ),
        ChoiceTwo(
          onTap: (x) {
            setState(() {
              _b = x;
            });

            _pageController.nextPage(
                duration: Duration(
                  milliseconds: 100,
                ),
                curve: Curves.easeInCirc);
          },
        ),
        ChoiceThree(
          onTap: (x) {
            setState(() {
              _c = x;
            });

            _pageController.nextPage(
                duration: Duration(
                  milliseconds: 100,
                ),
                curve: Curves.easeInCirc);
          },
        ),
        ChoiceFour(
          onTap: (x) {
            setState(() {
              _d = x;
            });

            _pageController.nextPage(
                duration: Duration(
                  milliseconds: 100,
                ),
                curve: Curves.easeInCirc);
          },
        ),
        ChoiceFive(
          onTap: (x) {
            _five = x;

            _pageController.nextPage(
                duration: Duration(
                  milliseconds: 100,
                ),
                curve: Curves.easeInCirc);
          },
        ),
        ChoiceSix(onTap: (x) {
          _six = x;
          _pageController.nextPage(
              duration: Duration(
                milliseconds: 100,
              ),
              curve: Curves.easeInCirc);
        }),
        ChoiceSeven(
          onTap: (x) {
            _seven = x;
            _finalize();
            _pageController.nextPage(
                duration: Duration(
                  milliseconds: 100,
                ),
                curve: Curves.easeInCirc);
          },
          reset: () {
            reset();
          },
        ),
      ];
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.white,
          title: CommonWidgets.masker(Text(
            "Calculate Your Risk",
            style: TextStyle(
              fontSize: 25,
              fontWeight: FontWeight.w900,
            ),
          )),
          centerTitle: true,
          leading: CommonWidgets.masker(
            IconButton(
              icon: Icon(CupertinoIcons.chevron_left),
              onPressed: () {
                popRoute(context);
              },
            ),
          ),
          actions: [
            _current == 7
                ? Container()
                : Center(
                    child: Text(
                      "${_current + 1}/7",
                      style: TextStyle(
                        fontSize: 18,
                        color: Colors.black,
                      ),
                    ),
                  ),
            SizedBox(
              width: 5,
            )
          ],
        ),
        body: PageView.builder(
            physics: NeverScrollableScrollPhysics(),
            controller: _pageController,
            itemCount: 8,
            onPageChanged: (x) {
              setState(() {
                _current = x;
              });
            },
            itemBuilder: (ctx, index) {
              return [
                ..._pages,
                ResultPage(
                  result: _result,
                  reCalculate: () {
                    reset();
                  },
                )
              ][index];
            }));
  }
}

String riskToWarning(double value) {
  if (value <= 1) {
    return "Low Chance";
  } else if (value > 1 && value <= 5) {
    return "Moderate Chance";
  } else if (value > 5 && value <= 10) {
    return "High Chance";
  }

  return "Extremely High Chance";
}
