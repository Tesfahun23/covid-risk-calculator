import 'package:covid_risk_calculator/ui/widgets/image-wrapper.dart';
import 'package:covid_risk_calculator/utils/ui/common/assets.dart';
import 'package:covid_risk_calculator/utils/ui/common/ui.dart';
import 'package:covid_risk_calculator/utils/ui/essentials.dart';
import 'package:flutter/material.dart';

typedef ProbablityCallback = Function(double result);
typedef MultiChoiceCallbakc = Function(List<double> result);

class ChoiceOne extends StatelessWidget {
  final ProbablityCallback onTap;

  const ChoiceOne({Key? key, required this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        child: Column(
      children: [
        SizedBox(
          height: 20,
        ),
        Center(
          child: Text(
            "Where is the event taking place?",
            textAlign: TextAlign.center,
            style: TextStyle(
                color: UiCommons.primaryColor,
                fontSize: 20,
                fontWeight: FontWeight.bold),
          ),
        ),
        InkWell(
          onTap: () {
            onTap(0.15);
          },
          child: ImageWrapperWidget(
            asset: ImageAssets.first_inside,
            height: calculateDimension(context, isHeight: true, percent: 0.25),
            title: "Inside",
          ),
        ),
        SizedBox(
          height: 20,
        ),
        InkWell(
          onTap: () {
            onTap(0.001);
          },
          child: ImageWrapperWidget(
            asset: ImageAssets.first_outside,
            height: calculateDimension(context, isHeight: true, percent: 0.25),
            title: "Outside",
          ),
        ),
        SizedBox(
          height: 50,
        )
      ],
    ));
  }
}
