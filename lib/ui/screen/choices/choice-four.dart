import 'package:covid_risk_calculator/ui/screen/choices/choice-one.dart';
import 'package:covid_risk_calculator/utils/ui/common/assets.dart';
import 'package:flutter/material.dart';

import 'package:covid_risk_calculator/utils/ui/common/ui.dart';
import 'package:covid_risk_calculator/utils/ui/essentials.dart';

class ChoiceFour extends StatefulWidget {
  final ProbablityCallback onTap;

  ChoiceFour({Key? key, required this.onTap}) : super(key: key);

  @override
  _ChoiceFourState createState() => _ChoiceFourState();
}

class _ChoiceFourState extends State<ChoiceFour> {
  List<ImageWithPair> _list = [];

  @override
  void initState() {
    _list = [
      new ImageWithPair(
          value: 0.00038888888,
          image: ImageAssets.fourth_birthday,
          title: 'A birthday party'),
      new ImageWithPair(
          value: 0.00002, image: ImageAssets.fourth_gym, title: 'The gym'),
      new ImageWithPair(
          value: 0.00038888888,
          image: ImageAssets.fourth_school,
          title: 'School'),
      new ImageWithPair(
          value: 0.00002, image: ImageAssets.fourth_library, title: 'Library'),
      new ImageWithPair(
          value: 0.00038888888,
          image: ImageAssets.fourth_hangout,
          title: 'Casual group hangout'),
      new ImageWithPair(
          value: 0.00002,
          image: ImageAssets.fourth_mail,
          title: 'Just Grabbing the\n mail'),
      new ImageWithPair(
          value: 0.00038888888,
          image: ImageAssets.fourth_camping,
          title: 'A Camping trip'),
      new ImageWithPair(
          value: 0.00038888888,
          image: ImageAssets.fourth_sports,
          title: 'An outdoor sports game'),
      new ImageWithPair(
          value: 0.00023255813,
          image: ImageAssets.fourth_airplane,
          title: 'Flying on an airplane'),
      new ImageWithPair(
          value: 0.00038888888,
          image: ImageAssets.fourth_dorm,
          title: 'Living in a dorm room with a roomate'),
      new ImageWithPair(
          value: 1 / 10000,
          image: ImageAssets.fourth_in_person_class,
          title: 'Attending in-person class'),
    ];

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: [
        SliverPersistentHeader(
            pinned: true,
            delegate: PersistentHeaderWrapper(
              maxHeight: 131,
              minHeight: 120,
              child: Container(
                color: Colors.white,
                child: Column(
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    Center(
                      child: Text(
                        "What venue will you be visting?",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: UiCommons.primaryColor,
                            fontSize: 20,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Center(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20.0),
                        child: Text(
                          "If your venue is not listed, click on the venue which is most similar to your anticipated activity.",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: UiCommons.accentColor,
                              fontSize: 15,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )),
        SliverPadding(
          padding: const EdgeInsets.all(8.0),
          sliver: SliverGrid.count(
            childAspectRatio: .8,
            mainAxisSpacing: 20,
            crossAxisSpacing: 10,
            crossAxisCount: 2,
            children: _list
                .map((e) => InkWell(
                      onTap: () {
                        widget.onTap(e.value);
                      },
                      child: Container(
                        clipBehavior: Clip.antiAlias,
                        margin: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                            border: Border.all(
                                color: UiCommons.accentColor, width: 1),
                            borderRadius: BorderRadius.circular(10)),
                        padding: EdgeInsets.all(15),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Container(
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  image: DecorationImage(
                                      image: AssetImage(e.image))),
                              height: 100,
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            Text(
                              e.title,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                letterSpacing: 1,
                                fontSize: 12,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            SizedBox(
                              height: 5,
                            )
                          ],
                        ),
                      ),
                    ))
                .toList(),
          ),
        ),
        SliverToBoxAdapter(
          child: SizedBox(
            height: 100,
          ),
        )
      ],
    );
  }
}

class ImageWithPair {
  final String image;
  final String title;
  final double value;
  ImageWithPair({
    required this.image,
    required this.title,
    this.value = 0,
  });
}
