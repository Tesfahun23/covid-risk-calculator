import 'dart:math';

import 'package:covid_risk_calculator/ui/screen/test-screen.dart';
import 'package:covid_risk_calculator/ui/widgets/le-button.dart';
import 'package:covid_risk_calculator/utils/ui/common/widgets.dart';
import 'package:flutter/material.dart';

extension Round on double {
  double roundToPrecision(int n) {
    num fac = pow(10, n);
    return (this * fac).round() / fac;
  }
}

class ResultPage extends StatelessWidget {
  final double result;
  final VoidCallback reCalculate;

  const ResultPage({Key? key, required this.result, required this.reCalculate})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: [
        SliverToBoxAdapter(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(
                height: 25,
              ),
              Text(
                "Your Risk",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 50,
                    color: Colors.purple,
                    letterSpacing: 2,
                    fontWeight: FontWeight.bold),
              ),
              CommonWidgets.masker(
                Text(
                  (result.roundToPrecision(4)).toString() + "%",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 70,
                      letterSpacing: 2,
                      fontWeight: FontWeight.bold),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: Text(
                  riskToWarning(result),
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Text(
                  "Based on the information you provided, we can best estimate you have a Low risk of being exposed to and catch COVID-19",
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15.0),
                child: Text(
                  "Despite receiving a low, moderate, or high risk, it’s important to understand that nearly any activity you complete or any event you attend may lead to a COVID exposure/infection. Always take necessary measures to keep yourself safe. Follow state, local, and CDC guidelines. This tool is not designed to replace medical advice and information from a medical professional. Always check with your medical professional to discuss risks before attending an event or completing an activity.",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 16,
                      color: Colors.purple.withOpacity(0.8),
                      fontWeight: FontWeight.bold),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              LeButton(onTap: reCalculate, label: "Recalculate")
            ],
          ),
        ),
        SliverToBoxAdapter(
          child: SizedBox(
            height: 100,
          ),
        )
      ],
    );
  }
}
