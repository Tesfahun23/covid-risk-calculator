import 'package:covid_risk_calculator/ui/screen/choices/choice-one.dart';
import 'package:covid_risk_calculator/utils/ui/common/ui.dart';
import 'package:covid_risk_calculator/utils/ui/common/widgets.dart';
import 'package:covid_risk_calculator/utils/ui/essentials.dart';
import 'package:flutter/material.dart';

class ChoiceThree extends StatefulWidget {
  final ProbablityCallback onTap;

  const ChoiceThree({Key? key, required this.onTap}) : super(key: key);

  @override
  State<ChoiceThree> createState() => _ChoiceThreeState();
}

class _ChoiceThreeState extends State<ChoiceThree> {
  List<List<int>> _choices = [
    [1, 10],
    [11, 20],
    [21, 30],
    [31, 50],
    [51, 100],
    [101, 250],
    [251, 1000],
    [1001, 2000],
    [2001, 5000],
    [5000]
  ];

  String _update(List<int> value) {
    bool _morethanTwo = value.length == 2;

    if (_morethanTwo) {
      return "${value.first} to ${value.last}";
    } else {
      return "more than 5000";
    }
  }

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: [
        SliverPersistentHeader(
            pinned: true,
            delegate: PersistentHeaderWrapper(
              maxHeight: 201,
              minHeight: 200,
              child: Container(
                color: Colors.white,
                child: Column(
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    CommonWidgets.masker(Icon(
                      Icons.group,
                      size: 80,
                    )),
                    SizedBox(
                      height: 20,
                    ),
                    Center(
                      child: Text(
                        "How many people will you be in close contact with?",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: UiCommons.primaryColor,
                            fontSize: 20,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    )
                  ],
                ),
              ),
            )),
        SliverPadding(
          padding: const EdgeInsets.all(8.0),
          sliver: SliverGrid.count(
            childAspectRatio: 1.2,
            mainAxisSpacing: 20,
            crossAxisSpacing: 20,
            crossAxisCount: 2,
            children: _choices
                .map((e) => InkWell(
                      onTap: () {
                        var _last = e.last;

                        if (_last > 2000) {
                          widget.onTap(0.99);
                        } else {
                          widget.onTap(_last * .000106);
                        }
                      },
                      child: Container(
                        margin: EdgeInsets.all(10),
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          border: Border.all(
                              width: 0.3, color: UiCommons.primaryColor),
                          borderRadius: BorderRadius.circular(15),
                          color: Color(0xffeeeeee).withOpacity(0.3),
                        ),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            SizedBox(
                              height: 8,
                            ),
                            CommonWidgets.masker(Icon(
                              Icons.group,
                              size: 25,
                            )),
                            SizedBox(
                              height: 8,
                            ),
                            Center(
                              child: Text(
                                _update(e),
                                style: TextStyle(
                                    color: UiCommons.primaryColor,
                                    fontSize: 13,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Center(
                              child: Text(
                                "People",
                                style: TextStyle(
                                    color: UiCommons.primaryColor,
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ))
                .toList(),
          ),
        ),
        SliverToBoxAdapter(
          child: SizedBox(
            height: 100,
          ),
        )
      ],
    );
  }
}
