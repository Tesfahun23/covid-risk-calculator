import 'package:covid_risk_calculator/ui/screen/choices/choice-five.dart';
import 'package:covid_risk_calculator/ui/screen/choices/choice-four.dart';
import 'package:covid_risk_calculator/ui/screen/choices/choice-one.dart';
import 'package:covid_risk_calculator/ui/widgets/le-button.dart';
import 'package:covid_risk_calculator/utils/ui/common/assets.dart';
import 'package:covid_risk_calculator/utils/ui/common/ui.dart';
import 'package:covid_risk_calculator/utils/ui/essentials.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ChoiceSeven extends StatefulWidget {
  final MultiChoiceCallbakc onTap;

  final VoidCallback reset;

  ChoiceSeven({Key? key, required this.onTap, required this.reset})
      : super(key: key);

  @override
  _ChoiceSevenState createState() => _ChoiceSevenState();
}

class _ChoiceSevenState extends State<ChoiceSeven> {
  List<MultiChoiceModel> _choosen = [];
  List<MultiChoiceModel> _choices = [];
  double _value = 0;

  @override
  void initState() {
    _choices = [
      MultiChoiceModel(
        value: 0.2,
        pair: new ImageWithPair(
            image: ImageAssets.seven_social, title: "Social Distancing"),
      ),
      MultiChoiceModel(
        value: 0.4,
        pair: new ImageWithPair(
            image: ImageAssets.seven_mask,
            title: "Wearing a cloth or medical mask"),
      ),
    ];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: [
        SliverPersistentHeader(
            pinned: true,
            delegate: PersistentHeaderWrapper(
              maxHeight: 81,
              minHeight: 80,
              child: Container(
                color: Colors.white,
                child: Column(
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    Center(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 0.0),
                        child: Text(
                          "Have you been taking any precautions?",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: UiCommons.primaryColor,
                              fontSize: 20,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )),
        SliverPadding(
          padding: const EdgeInsets.all(8.0),
          sliver: SliverGrid.count(
            childAspectRatio: .8,
            mainAxisSpacing: 20,
            crossAxisSpacing: 10,
            crossAxisCount: 2,
            children: _choices
                .map((e) => InkWell(
                      onTap: () {
                        if (_choosen.contains(e)) {
                          setState(() {
                            _choosen.remove(e);
                            _value = _value - e.value;
                          });
                        } else {
                          setState(() {
                            _choosen.add(e);
                            _value = _value + e.value;
                          });
                        }
                      },
                      child: Container(
                        clipBehavior: Clip.antiAlias,
                        margin: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                            border: Border.all(
                                color: UiCommons.accentColor, width: 1),
                            borderRadius: BorderRadius.circular(10)),
                        padding: EdgeInsets.all(15),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Row(
                              children: [
                                Spacer(),
                                Icon(
                                  Icons.check_circle,
                                  color: _choosen.contains(e)
                                      ? UiCommons.accentColor
                                      : Colors.grey,
                                )
                              ],
                            ),
                            Container(
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  image: DecorationImage(
                                      image: AssetImage(e.pair.image))),
                              height: 100,
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            Text(
                              e.pair.title,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                letterSpacing: 1,
                                fontSize: 12,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            SizedBox(
                              height: 5,
                            )
                          ],
                        ),
                      ),
                    ))
                .toList(),
          ),
        ),
        SliverToBoxAdapter(
            child: Column(
          children: [
            LeButton(
                onTap: () {
                  widget.onTap(_choosen.map((e) => e.value).toList());
                },
                label: "Calculate"),
            SizedBox(
              height: 8,
            ),
            LeButton(
                color: CupertinoColors.destructiveRed,
                onTap: () {
                  widget.reset();
                },
                label: "Reset"),
            SizedBox(
              height: 50,
            ),
          ],
        ))
      ],
    );
  }
}
