import 'package:covid_risk_calculator/ui/screen/choices/choice-one.dart';
import 'package:covid_risk_calculator/ui/widgets/image-wrapper.dart';
import 'package:covid_risk_calculator/utils/ui/common/assets.dart';
import 'package:covid_risk_calculator/utils/ui/common/ui.dart';
import 'package:covid_risk_calculator/utils/ui/essentials.dart';
import 'package:flutter/material.dart';

class ChoiceTwo extends StatelessWidget {
  final ProbablityCallback onTap;
  const ChoiceTwo({Key? key, required this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        child: Column(
      children: [
        SizedBox(
          height: 20,
        ),
        Center(
          child: Text(
            "Will you be in close contact with someone?",
            textAlign: TextAlign.center,
            style: TextStyle(
                color: UiCommons.primaryColor,
                fontSize: 20,
                fontWeight: FontWeight.bold),
          ),
        ),
        InkWell(
          onTap: () {
            onTap(0.00038888888);
          },
          child: ImageWrapperWidget(
            asset: ImageAssets.second_yes,
            height: calculateDimension(context, isHeight: true, percent: 0.25),
            title: "Yes",
          ),
        ),
        SizedBox(
          height: 20,
        ),
        InkWell(
          onTap: () {
            onTap(0.00038888888);
          },
          child: ImageWrapperWidget(
            asset: ImageAssets.second_no,
            height: calculateDimension(context, isHeight: true, percent: 0.25),
            title: "No",
          ),
        ),
        SizedBox(
          height: 50,
        )
      ],
    ));
  }
}
