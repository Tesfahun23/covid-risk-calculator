import 'package:covid_risk_calculator/utils/ui/common/ui.dart';
import 'package:covid_risk_calculator/utils/ui/common/widgets.dart';
import 'package:covid_risk_calculator/utils/ui/nav.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'package:url_launcher/url_launcher.dart';

class AboutScreen extends StatelessWidget {
  const AboutScreen({Key? key}) : super(key: key);

  Future<void> customLaunch(command) async {
    if (await canLaunch(command)) {
      await launch(command);
    } else {
      Logger().e(' could not launch $command');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        title: CommonWidgets.masker(Text(
          "About",
          style: TextStyle(
            fontSize: 25,
            fontWeight: FontWeight.w900,
          ),
        )),
        centerTitle: true,
        leading: CommonWidgets.masker(
          IconButton(
            icon: Icon(CupertinoIcons.chevron_left),
            onPressed: () {
              popRoute(context);
            },
          ),
        ),
        actions: [],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.only(bottom: 10),
              decoration: BoxDecoration(
                  border: Border(
                      bottom: BorderSide(
                          width: 0.2, color: UiCommons.primaryColor))),
              child: ListTile(
                onTap: () async {
                  await customLaunch(
                      "https://stephenjosey.com/COVID-19/details.html");
                },
                leading: Icon(
                  Icons.info,
                  color: UiCommons.primaryColor,
                ),
                subtitle: Text("tap to view"),
                title: Text("How the app works"),
              ),
            ),
            Container(
              padding: EdgeInsets.only(bottom: 10),
              decoration: BoxDecoration(
                  border: Border(
                      bottom: BorderSide(
                          width: 0.2, color: UiCommons.primaryColor))),
              child: ListTile(
                onTap: () async {
                  await customLaunch(
                      "https://stephenjosey.com/COVID-19/references.html");
                },
                leading: Icon(
                  Icons.info,
                  color: UiCommons.primaryColor,
                ),
                subtitle: Text("tap to view"),
                title: Text("References & details"),
              ),
            ),
            Container(
              padding: EdgeInsets.only(bottom: 10),
              decoration: BoxDecoration(
                  border: Border(
                      bottom: BorderSide(
                          width: 0.2, color: UiCommons.primaryColor))),
              child: ListTile(
                onTap: () async {
                  await customLaunch(
                      "https://stephenjosey.com/COVID-19/termsofservice.html");
                },
                leading: Icon(
                  Icons.info,
                  color: UiCommons.primaryColor,
                ),
                subtitle: Text("Tap to view"),
                title: Text("Terms of service"),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
