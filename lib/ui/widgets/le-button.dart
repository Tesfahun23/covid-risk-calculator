import 'package:covid_risk_calculator/utils/ui/common/ui.dart';
import 'package:covid_risk_calculator/utils/ui/essentials.dart';
import 'package:flutter/material.dart';

class LeButton extends StatelessWidget {
  final double radius;
  final VoidCallback onTap;
  final String label;
  final double? width;
  final Color? color;
  const LeButton(
      {Key? key,
      this.radius = 30,
      this.width,
      this.color,
      required this.onTap,
      required this.label})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      splashColor: Colors.white,
      child: Container(
        height: 55,
        alignment: Alignment.center,
        child: Text(
          label,
          style: TextStyle(
              color: Colors.white, fontSize: 18, fontWeight: FontWeight.w900),
        ),
        clipBehavior: Clip.antiAlias,
        padding: EdgeInsets.symmetric(vertical: 10),
        margin: EdgeInsets.all(10),
        width:
            width ?? calculateDimension(context, isHeight: false, percent: 0.5),
        decoration: BoxDecoration(
            color: color,
            borderRadius: BorderRadius.circular(radius),
            gradient: color != null ? null : UiCommons.leftToRight()),
      ),
    );
  }
}
