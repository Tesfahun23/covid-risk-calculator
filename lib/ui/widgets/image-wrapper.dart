import 'package:covid_risk_calculator/utils/ui/common/ui.dart';
import 'package:flutter/material.dart';

class ImageWrapperWidget extends StatelessWidget {
  final double height;
  final String asset;
  final String title;
  const ImageWrapperWidget({
    Key? key,
    required this.height,
    required this.asset,
    required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      clipBehavior: Clip.antiAlias,
      margin: EdgeInsets.all(10),
      decoration: BoxDecoration(
          border: Border.all(color: UiCommons.accentColor, width: 1),
          borderRadius: BorderRadius.circular(10)),
      padding: EdgeInsets.all(15),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            decoration: BoxDecoration(
                color: Colors.white,
                image: DecorationImage(image: AssetImage(asset))),
            height: height,
          ),
          SizedBox(
            height: 8,
          ),
          Text(
            title,
            textAlign: TextAlign.center,
            style: TextStyle(
              letterSpacing: 1,
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(
            height: 10,
          )
        ],
      ),
    );
  }
}
