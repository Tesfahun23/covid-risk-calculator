import 'package:covid_risk_calculator/utils/ui/common/ui.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

// Date Picker typedef
typedef DatePickerChanged = void Function(DateTime? date);

//
MaterialColor primarySwatch =
    new MaterialColor(0xff009DE6, swatchColorMaker(UiCommons.primaryColor));

Map<int, Color> swatchColorMaker(Color color) {
  return <int, Color>{
    50: color.withOpacity(0.1),
    100: color.withOpacity(0.2),
    200: color.withOpacity(0.3),
    300: color.withOpacity(0.4),
    400: color.withOpacity(0.5),
    500: color.withOpacity(0.6),
    600: color.withOpacity(0.7),
    700: color.withOpacity(0.8),
    800: color.withOpacity(0.9),
    900: color.withOpacity(1),
  };
}

final BorderRadius buttonRadius = BorderRadius.circular(30);

class PersistentHeaderWrapper extends SliverPersistentHeaderDelegate {
  final Widget? child;
  final double? maxHeight;
  final double? minHeight;

  PersistentHeaderWrapper({this.child, this.maxHeight, this.minHeight});
  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return LayoutBuilder(builder: (context, constraints) {
      return child!;
    });
  }

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate _) => true;

  @override
  double get maxExtent => maxHeight!;

  @override
  double get minExtent => minHeight!;
}

double calculateDimension(BuildContext context,
    {required bool isHeight, required double percent}) {
  Size size = MediaQuery.of(context).size;
  return isHeight ? size.height * percent : size.width * percent;
}

void showSnackBar(BuildContext context,
    {required String message,
    Color textColor = Colors.white,
    background: CupertinoColors.destructiveRed}) {
  final snackBar = SnackBar(
    elevation: 0.3,
    behavior: SnackBarBehavior.floating,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
    backgroundColor: background,
    content: Text(
      message,
      style: TextStyle(color: textColor),
    ),
  );

  ScaffoldMessenger.of(context).showSnackBar(snackBar);
}
