import 'package:flutter/material.dart';

abstract class UiCommons {
  static const Color primaryColor = Color(0xff009DE6);
  static const Color accentColor = Color(0xff8169BB);

  static const Color grayColor = Colors.grey;
  static const Color whiteColor = Colors.white;

// TextField Definitions
  static double lableFontSize() => 13;
  static OutlineInputBorder textFieldIdle() => OutlineInputBorder(
      borderRadius: BorderRadius.circular(10),
      borderSide: BorderSide(color: Colors.grey.withOpacity(0.7), width: 0.5));
  static OutlineInputBorder textFieldfocused() => OutlineInputBorder(
      borderRadius: BorderRadius.circular(10),
      borderSide: BorderSide(color: Colors.grey.withOpacity(0.7), width: 0.5));
  static OutlineInputBorder errorrTextFieldBorder() => OutlineInputBorder(
      borderRadius: BorderRadius.circular(10),
      borderSide: BorderSide(color: Colors.red.withOpacity(0.5), width: 0.5));

// Gradients
  static LinearGradient leftToRight() => LinearGradient(
      begin: Alignment.centerRight,
      end: Alignment.centerLeft,
      tileMode: TileMode.clamp,
      colors: [accentColor, primaryColor]);
  static LinearGradient topToBottom() => LinearGradient(
      begin: Alignment.topCenter,
      end: Alignment.bottomCenter,
      colors: [accentColor, primaryColor]);

  static LinearGradient rightCornerToLeftCorner() => LinearGradient(
      begin: Alignment.topRight,
      end: Alignment.bottomLeft,
      tileMode: TileMode.mirror,
      colors: [accentColor, primaryColor, primaryColor]);
}
