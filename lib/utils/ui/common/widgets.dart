import 'package:covid_risk_calculator/utils/ui/common/ui.dart';
import 'package:flutter/material.dart';

abstract class CommonWidgets {
  static Widget masker(Widget widget, {LinearGradient? gradient}) {
    return ShaderMask(
        child: widget,
        blendMode: BlendMode.srcATop,
        shaderCallback: (Rect bounds) {
          return !(gradient == null)
              ? gradient.createShader(bounds)
              : UiCommons.leftToRight().createShader(bounds);
        });
  }

  static Widget emptyList(
      {required IconData icon,
      double iconSize = 80,
      required String msg,
      double fontSize = 16}) {
    return Container(
      alignment: Alignment.center,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(
            icon,
            size: iconSize,
            color: UiCommons.accentColor.withOpacity(0.5),
          ),
          SizedBox(height: 20),
          Text(
            msg,
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: fontSize,
                color: UiCommons.primaryColor.withOpacity(0.4)),
          )
        ],
      ),
    );
  }

  static Widget emptyWidget(
      {required String msg, String? asset, double fontSize = 16}) {
    return Container(
      alignment: Alignment.center,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Center(
            child: Image.asset(
              asset ?? "assets/no.png",
              height: 250,
              width: 250,
            ),
          ),
          SizedBox(height: 20),
          Text(
            msg,
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: fontSize,
                color: UiCommons.primaryColor.withOpacity(0.9)),
          )
        ],
      ),
    );
  }
}
