// ignore_for_file: non_constant_identifier_names

abstract class ImageAssets {
  static final String first_inside = "assets/img/first/inside.png";
  static final String first_outside = "assets/img/first/outside.png";

  //second
  static final String second_yes = "assets/img/second/yes.png";
  static final String second_no = "assets/img/second/no.png";

// fourth

  static final String fourth_airplane = "assets/img/four/airplane.png";
  static final String fourth_birthday = "assets/img/four/bday.png";
  static final String fourth_camping = "assets/img/four/camping.png";
  static final String fourth_dorm = "assets/img/four/dorm.png";
  static final String fourth_gym = "assets/img/four/gym.png";
  static final String fourth_hangout = "assets/img/four/hangout.png";
  static final String fourth_in_person_class =
      "assets/img/four/in-person-class.png";
  static final String fourth_library = "assets/img/four/library.png";
  static final String fourth_mail = "assets/img/four/mail.png";
  static final String fourth_school = "assets/img/four/school.png";
  static final String fourth_sports = "assets/img/four/sports.png";

  //five

  static final String fifth_eating = "assets/img/five/eating.png";
  static final String fifth_physical = "assets/img/five/physical.png";
  static final String fifth_singing = "assets/img/five/singing.png";
  static final String fifth_speaking = "assets/img/five/speaking.png";
  static final String fifth_strong_physical =
      "assets/img/five/strong-physical.png";

  // Six
  static final String six_surfaces = "assets/img/six/surface.png";
  static final String six_ventilation = "assets/img/six/vent.png";

  // Seven
  static final String seven_mask = "assets/img/seven/mask.png";
  static final String seven_social = "assets/img/seven/social.png";
}
