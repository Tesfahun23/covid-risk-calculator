import 'package:flutter/material.dart';

void navigateToScreen(BuildContext context,
    {required Widget screen, bool replace: false}) {
  if (replace) {
    Navigator.of(context).pushReplacement(
        new MaterialPageRoute(builder: (BuildContext _) => screen));
  } else {
    Navigator.of(context)
        .push(new MaterialPageRoute(builder: (BuildContext context) => screen));
  }
}

void popRoute(BuildContext context) => Navigator.pop(context);
