import 'package:gsheets/gsheets.dart';

var _key = r'''
  
  {
  "type": "service_account",
  "project_id": "gsheets-334504",
  "private_key_id": "861af7f39b17ec3e04fabffe04e6bb56c16a0429",
  "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQCJd8R9uw4Br2s7\nRTh/8N6VWhl+DxRPFcUuzsAwitDv64x3+qZMyaRauJ2a7OL6UzUqjcpLbMszMaxc\nU99KB4w7CO1T1vIo/tyim8iabdrI9Rdot3FNsqFBCZF53k7/wWjNHI5+hBFtjGoS\nwrtapjhWWP2E/jmrmH/3FYyK88SkCyrRF1EAOi1hiYDRbKC+pr7Xk4Q0CMVdTfMi\nltfOmYnm831y07LIOUnVzLziRppsRRJme5kDuV9zjafcFulYJac9M9IzVIFaNvMk\nKlF+TAmeO3Ju5YDmujNHyh9Nj+Ql//r4LYoK5l5g3QNRnkSMLEHkOHJd0Y3T3YwD\ncJDIUwvBAgMBAAECggEAAp6utvq2pJ+UOpfyILYVd+E0tStSZsNyc5brIMG0g64I\nikzTmIkLQdNc5ZP4K28kh0AWh+N15+7daxEKI14LdSCRbVSeXtukltDDPPY1Ajlx\njcRZO3LZEeU9yYXMGaG0/pu5t32K23yelObzLSIX1sVxZ3R04ulQHwRhHlDRAZNR\nwgqF5QukMvBVDWusKrv0lIT7tJ96/5p03BOQPID9RapRfUzzvHe/ZvlXNkZ7G8/s\nFbJLXS1242zOqEphF3HYhgisKb1+k8plaB5t6JXB0Q+CKgZHwW40f9nEGYyZ4ZjQ\nEyvjcWObZvVyBlYJq6FhhLbWo7QYesAHiTf4NqFqVQKBgQC8klAf7ofeXrgG51VZ\nxWhaKcVt7+qozxc25lLCqHGGhDVrisZpXcb9En9iWHWYTDgmwnax0hL0EjCpuJ7B\nOCAuEd3AB2kGOqS3hRJU0Ve52sy8733WDq8xKH28u/9yJke9Bo2XOHB/dic987CI\nhdwd2SykUXOB+M91vk1dya5nYwKBgQC6n3ana/nKfEDGAbfZPAlyuV9cPVDQs6EE\nRgR6zCGAExhtZgOlW8t8XyCiJkRTBqDs8tpCGejQc0Hrq/fFXlfgFfrSV9oy6uaj\nOkj3wOeFgFe0+n9IhT7g6BvRmC80Q6AfnMb4gRRZx3zLWwvc4wx44PSSSu+IgWGn\nVW5VGcZDiwKBgQCt9SPpcTjyENsfs0TvfccYdjRliNAnTnQR90QdwxqHh2tofCsx\nhItPZ0MQTJ6fEqEyUG8C99L4SdeOmnwJr2cwWNcCkVN3pZ3SlvYkPML69zesjBq/\n3+gKJ1pnQ98RRPMlE+iGulA9pZfp9ObD6wj2mCQwqdsu6YMpEZd5j9ZOFQKBgQCQ\ncQPBKU04b1si5gmJ1XHqnZc1KS/3mFAzoo2tDXmQSzdI1QBVZ+ri5C250qxnJmY/\nfud26d7MF6kZ4wvucsrI4bo0O1LivfSsuVvohiGrrl4z8P26/4ZfRMbjPkqqxPyV\n+x31ENLTehcYves8l2770AD7iYxDtwACJ5kxvLU7aQKBgQCfvTI38XrVO50dM1sz\nl6Pg1UvYicT//mLAaEvfA3nmU4l3m9Sj9ZzAbaJ9irMzWEZVC/OVLdGT/lzMeWI4\naY8i42hM7yk2Z0N/SYh8YmrWrdpXY28SNU8bSpfTwZ2BdSs87AKVRm0gurF/OWu3\n26hwHTEWJ5Sd8w7+ffrcI0iYlw==\n-----END PRIVATE KEY-----\n",
  "client_email": "gsheets@gsheets-334504.iam.gserviceaccount.com",
  "client_id": "105864633333407322695",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://oauth2.googleapis.com/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/gsheets%40gsheets-334504.iam.gserviceaccount.com"
}


  ''';

final gsheets = GSheets(_key);

Future addResultToSheet(double result) async {
  final ss =
      await gsheets.spreadsheet("1XmlGeXn6o1BFsPrB2GKI3ruMj9zM0tEa-P7MKq90SiU");

  var sheet = ss.worksheetByTitle('Sheet1');
  sheet ??= await ss.addWorksheet('Sheet1');

  await sheet.values.insertRow(
    2,
    [result, DateTime.now().toString()],
  );
}
