import 'package:covid_risk_calculator/data/database/database.dart';
import 'package:get_it/get_it.dart';

final getIt = GetIt.instance;

void setup() {
  getIt.registerSingleton<CovidDatabase>(CovidDatabase());
}
