import 'package:covid_risk_calculator/ui/screen/splash-screen.dart';
import 'package:covid_risk_calculator/utils/getIt.dart';
import 'package:flutter/material.dart';

void main() {
  setup();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Covid-19 Risk Calculator',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        scaffoldBackgroundColor: Colors.white,
        fontFamily: "OpenSans",
        primarySwatch: Colors.blue,
      ),
      home: SplashScreen(),
    );
  }
}
