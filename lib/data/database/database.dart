import 'package:drift/native.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart' as p;
import 'package:drift/drift.dart';
import 'dart:io';

part 'database.g.dart';

LazyDatabase _openConnection() {
  return LazyDatabase(() async {
    final dbFolder = await getApplicationDocumentsDirectory();
    final file = File(p.join(dbFolder.path, 'covid.sqlite'));
    return NativeDatabase(file);
  });
}

class Results extends Table {
  IntColumn get id => integer().autoIncrement()();
  RealColumn get result => real()();
  DateTimeColumn get date => dateTime()();
}

@DriftDatabase(tables: [Results], daos: [ResultsDao])
class CovidDatabase extends _$CovidDatabase {
  CovidDatabase() : super(_openConnection());

  @override
  int get schemaVersion => 1;
}

@DriftAccessor(tables: [Results])
class ResultsDao extends DatabaseAccessor<CovidDatabase>
    with _$ResultsDaoMixin {
  ResultsDao(CovidDatabase db) : super(db);

  Stream<List<Result>> all() {
    return (select(results)..orderBy([(x) => OrderingTerm.desc(x.date)]))
        .watch();
  }

  Future addResult(ResultsCompanion result) async {
    return into(results).insert(result);
  }
}
