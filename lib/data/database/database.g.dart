// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'database.dart';

// **************************************************************************
// MoorGenerator
// **************************************************************************

// ignore_for_file: unnecessary_brace_in_string_interps, unnecessary_this
class Result extends DataClass implements Insertable<Result> {
  final int id;
  final double result;
  final DateTime date;
  Result({required this.id, required this.result, required this.date});
  factory Result.fromData(Map<String, dynamic> data, {String? prefix}) {
    final effectivePrefix = prefix ?? '';
    return Result(
      id: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}id'])!,
      result: const RealType()
          .mapFromDatabaseResponse(data['${effectivePrefix}result'])!,
      date: const DateTimeType()
          .mapFromDatabaseResponse(data['${effectivePrefix}date'])!,
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['result'] = Variable<double>(result);
    map['date'] = Variable<DateTime>(date);
    return map;
  }

  ResultsCompanion toCompanion(bool nullToAbsent) {
    return ResultsCompanion(
      id: Value(id),
      result: Value(result),
      date: Value(date),
    );
  }

  factory Result.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return Result(
      id: serializer.fromJson<int>(json['id']),
      result: serializer.fromJson<double>(json['result']),
      date: serializer.fromJson<DateTime>(json['date']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'result': serializer.toJson<double>(result),
      'date': serializer.toJson<DateTime>(date),
    };
  }

  Result copyWith({int? id, double? result, DateTime? date}) => Result(
        id: id ?? this.id,
        result: result ?? this.result,
        date: date ?? this.date,
      );
  @override
  String toString() {
    return (StringBuffer('Result(')
          ..write('id: $id, ')
          ..write('result: $result, ')
          ..write('date: $date')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, result, date);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is Result &&
          other.id == this.id &&
          other.result == this.result &&
          other.date == this.date);
}

class ResultsCompanion extends UpdateCompanion<Result> {
  final Value<int> id;
  final Value<double> result;
  final Value<DateTime> date;
  const ResultsCompanion({
    this.id = const Value.absent(),
    this.result = const Value.absent(),
    this.date = const Value.absent(),
  });
  ResultsCompanion.insert({
    this.id = const Value.absent(),
    required double result,
    required DateTime date,
  })  : result = Value(result),
        date = Value(date);
  static Insertable<Result> custom({
    Expression<int>? id,
    Expression<double>? result,
    Expression<DateTime>? date,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (result != null) 'result': result,
      if (date != null) 'date': date,
    });
  }

  ResultsCompanion copyWith(
      {Value<int>? id, Value<double>? result, Value<DateTime>? date}) {
    return ResultsCompanion(
      id: id ?? this.id,
      result: result ?? this.result,
      date: date ?? this.date,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (result.present) {
      map['result'] = Variable<double>(result.value);
    }
    if (date.present) {
      map['date'] = Variable<DateTime>(date.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('ResultsCompanion(')
          ..write('id: $id, ')
          ..write('result: $result, ')
          ..write('date: $date')
          ..write(')'))
        .toString();
  }
}

class $ResultsTable extends Results with TableInfo<$ResultsTable, Result> {
  final GeneratedDatabase _db;
  final String? _alias;
  $ResultsTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  late final GeneratedColumn<int?> id = GeneratedColumn<int?>(
      'id', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultConstraints: 'PRIMARY KEY AUTOINCREMENT');
  final VerificationMeta _resultMeta = const VerificationMeta('result');
  late final GeneratedColumn<double?> result = GeneratedColumn<double?>(
      'result', aliasedName, false,
      typeName: 'REAL', requiredDuringInsert: true);
  final VerificationMeta _dateMeta = const VerificationMeta('date');
  late final GeneratedColumn<DateTime?> date = GeneratedColumn<DateTime?>(
      'date', aliasedName, false,
      typeName: 'INTEGER', requiredDuringInsert: true);
  @override
  List<GeneratedColumn> get $columns => [id, result, date];
  @override
  String get aliasedName => _alias ?? 'results';
  @override
  String get actualTableName => 'results';
  @override
  VerificationContext validateIntegrity(Insertable<Result> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('result')) {
      context.handle(_resultMeta,
          result.isAcceptableOrUnknown(data['result']!, _resultMeta));
    } else if (isInserting) {
      context.missing(_resultMeta);
    }
    if (data.containsKey('date')) {
      context.handle(
          _dateMeta, date.isAcceptableOrUnknown(data['date']!, _dateMeta));
    } else if (isInserting) {
      context.missing(_dateMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Result map(Map<String, dynamic> data, {String? tablePrefix}) {
    return Result.fromData(data,
        prefix: tablePrefix != null ? '$tablePrefix.' : null);
  }

  @override
  $ResultsTable createAlias(String alias) {
    return $ResultsTable(_db, alias);
  }
}

abstract class _$CovidDatabase extends GeneratedDatabase {
  _$CovidDatabase(QueryExecutor e) : super(SqlTypeSystem.defaultInstance, e);
  late final $ResultsTable results = $ResultsTable(this);
  late final ResultsDao resultsDao = ResultsDao(this as CovidDatabase);
  @override
  Iterable<TableInfo> get allTables => allSchemaEntities.whereType<TableInfo>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities => [results];
}

// **************************************************************************
// DaoGenerator
// **************************************************************************

mixin _$ResultsDaoMixin on DatabaseAccessor<CovidDatabase> {
  $ResultsTable get results => attachedDatabase.results;
}
